$( document ).ready(function() {

manageData();

/* manage data list */
function manageData() {
   $.ajax({
       dataType: 'json',
       url: 'http://localhost:8080/Ajax/getMatches.jsp'
    
   }).done(function(data){
       manageRow(data);
   });
}


/* Add new Item table row */
function manageRow(data) {
    var    rows = '';
    $.each( data, function( key, value ) {
         rows = rows + '<div class = "match-body">';
               rows = rows + '<div class="team-one"><img src="img/' + value.team_a + '.png"></div>';
               rows = rows + '<div class="team-two"><img src="img/' + value.team_b + '.png"></div>';
               rows = rows + '<div class="match-stats">';
               rows = rows + '<p class ="competition">Eredivisie</p>';
               rows = rows + '<p class ="date">' + value.date_match + ' </p>';
               rows = rows + '<p class ="date">' + value.time_match+ ' </p>';
               rows = rows + '<p class ="xxxx">' + 
               value.tickets_available + ' </p>';
               rows = rows + '<a href="" class="buybutton">Buy tickets</a>   </div>';         
         rows = rows + '</div>'
    });

         
    $(".schedule").html(rows);
}

/*create new item */
    
    $(".submit").click(function(e){

 
   e.preventDefault();
   var form_action = $("#myForm").attr("action");
   var firstname = $("#myForm").find("input[name='firstname']").val();
   var lastname = $("#myForm").find("input[name='lastname']").val();
   var cardnumber = $("#myForm").find("input[name='cardnumber']").val();
   var email = $("#myForm").find("input[name='email']").val();
   var username = $("#myForm").find("input[name='username']").val();
   var password = $("#myForm").find("input[name='password']").val();
   
   
   console.log (firstname, lastname, cardnumber, email, username, password);

   if(firstname != '' && lastname != '' && cardnumber != '' && email != '' && username != '' && password != ''){
       $.ajax({
           dataType: 'json',
           type:'GET',
           url: form_action,
           data:{firstname:firstname, lastname:lastname, cardnumber:cardnumber, email:email, username:username, password:password}
       }).done(function(data){
           $("#myForm").find("input[name='firstname']").val('');
           $("#myForm").find("input[name='lastname']").val('');
           $("#myForm").find("input[name='cardnumber']").val('');
           $("#myForm").find("input[name='email']").val('');
           $("#myForm").find("input[name='username']").val('');
           $("#myForm").find("input[name='password']").val('');
           
           console.log(data.MESSAGE);
           if (data.MESSAGE == "OK"){
               $("#myForm").hide();
               $("#myModalOk").show();

           }else{
               $("#myForm").hide();
               $("#myModalKo").show();       
           }      
       });

   }else{
       alert('You are missing title or description.')
   }

});

});

